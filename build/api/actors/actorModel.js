"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var Schema = _mongoose["default"].Schema;
var ActorSchema = new Schema({
  id: {
    type: Number,
    unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  place_of_birth: {
    type: String,
    required: true
  },
  known_for_department: {
    type: String,
    required: true
  }
});

var _default = _mongoose["default"].model('Actor', ActorSchema);

exports["default"] = _default;