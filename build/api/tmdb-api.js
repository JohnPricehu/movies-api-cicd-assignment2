"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUpcomingMovies = exports.getTopRatedMovies = exports.getPerson = exports.getNowPlayingMovies = exports.getMovies = exports.getMovieSimilar = exports.getMovieReviews = exports.getMovieImages = exports.getMovieCredits = exports.getMovie = exports.getGenres = void 0;

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

var getUpcomingMovies = function getUpcomingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/upcoming?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getUpcomingMovies = getUpcomingMovies;

var getNowPlayingMovies = function getNowPlayingMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/now_playing?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getNowPlayingMovies = getNowPlayingMovies;

var getTopRatedMovies = function getTopRatedMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/top_rated?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getTopRatedMovies = getTopRatedMovies;

var getMovieSimilar = function getMovieSimilar(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/similar?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (res) {
    return res.json();
  }).then(function (json) {
    return json.results;
  });
};

exports.getMovieSimilar = getMovieSimilar;

var getMovieCredits = function getMovieCredits(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/credits?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovieCredits = getMovieCredits;

var getMovieImages = function getMovieImages(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/images?api_key=").concat(process.env.TMDB_KEY)).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovieImages = getMovieImages;

var getMovieReviews = function getMovieReviews(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "/reviews?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovieReviews = getMovieReviews;

var getPerson = function getPerson(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/person/".concat(id, "?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getPerson = getPerson;

var getMovies = function getMovies() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/discover/movie?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&include_adult=false&include_video=false&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovies = getMovies;

var getMovie = function getMovie(id) {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/movie/".concat(id, "?api_key=").concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getMovie = getMovie;

var getGenres = function getGenres() {
  return (0, _nodeFetch["default"])("https://api.themoviedb.org/3/genre/movie/list?api_key=".concat(process.env.TMDB_KEY, "&language=en-US&page=1")).then(function (response) {
    if (!response.ok) {
      throw new Error(response.json().message);
    }

    return response.json();
  })["catch"](function (error) {
    throw error;
  });
};

exports.getGenres = getGenres;