"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _express = _interopRequireDefault(require("express"));

var _uniqid = _interopRequireDefault(require("uniqid"));

var _movieModel = _interopRequireDefault(require("./movieModel"));

var _expressAsyncHandler = _interopRequireDefault(require("express-async-handler"));

var _tmdbApi = require("../tmdb-api");

// import { movies, movieReviews, movieDetails } from './moviesData';
var router = _express["default"].Router();

router.get('/', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
    var _req$query, _req$query$page, page, _req$query$limit, limit, _ref2, totalDocumentsPromise, moviesPromise, totalDocuments, movies, returnObject;

    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _req$query = req.query, _req$query$page = _req$query.page, page = _req$query$page === void 0 ? 1 : _req$query$page, _req$query$limit = _req$query.limit, limit = _req$query$limit === void 0 ? 10 : _req$query$limit; // destructure page and limit and set default values

            _ref2 = [+page, +limit];
            page = _ref2[0];
            limit = _ref2[1];
            //trick to convert to numeric (req.query will contain string values)
            totalDocumentsPromise = _movieModel["default"].estimatedDocumentCount(); //Kick off async calls

            moviesPromise = _movieModel["default"].find().limit(limit).skip((page - 1) * limit);
            _context.next = 8;
            return totalDocumentsPromise;

          case 8:
            totalDocuments = _context.sent;
            _context.next = 11;
            return moviesPromise;

          case 11:
            movies = _context.sent;
            returnObject = {
              page: page,
              total_pages: Math.ceil(totalDocuments / limit),
              total_results: totalDocuments,
              results: movies
            }; //construct return Object and insert into response object

            res.status(200).json(returnObject);

          case 14:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}()));
router.get('/:id', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref3 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
    var id, movie;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            id = parseInt(req.params.id);
            _context2.next = 3;
            return _movieModel["default"].findByMovieDBId(id);

          case 3:
            movie = _context2.sent;

            if (movie) {
              res.status(200).json(movie);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref3.apply(this, arguments);
  };
}()));
router["delete"]('/:id', /*#__PURE__*/function () {
  var _ref4 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
    var id, movie;
    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            id = parseInt(req.params.id);
            _context3.next = 3;
            return _movieModel["default"].findByMovieDBId(id);

          case 3:
            movie = _context3.sent;

            if (movie) {
              _movieModel["default"].deleteOne({
                id: id
              });

              res.status(200).send("delete successfully");
            } else {
              res.status(404).send("Not find the moive to delete");
            }

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6) {
    return _ref4.apply(this, arguments);
  };
}());
router.get('/:id/credits', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref5 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
    var id, credits;
    return _regenerator["default"].wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            id = parseInt(req.params.id);
            _context4.next = 3;
            return (0, _tmdbApi.getMovieCredits)(id);

          case 3:
            credits = _context4.sent;

            if (credits) {
              res.status(200).json(credits);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x7, _x8) {
    return _ref5.apply(this, arguments);
  };
}()));
router.get('/:id/reviews', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref6 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
    var id, reviews;
    return _regenerator["default"].wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            id = parseInt(req.params.id);
            _context5.next = 3;
            return (0, _tmdbApi.getMovieReviews)(id);

          case 3:
            reviews = _context5.sent;

            if (reviews) {
              res.status(200).json(reviews);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x9, _x10) {
    return _ref6.apply(this, arguments);
  };
}()));
router.get('/:id/similar', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref7 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(req, res) {
    var id, similarMovie;
    return _regenerator["default"].wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            id = parseInt(req.params.id);
            _context6.next = 3;
            return (0, _tmdbApi.getMovieSimilar)(id);

          case 3:
            similarMovie = _context6.sent;

            if (similarMovie) {
              res.status(200).json(similarMovie);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x11, _x12) {
    return _ref7.apply(this, arguments);
  };
}()));
router.get('/:id/images', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref8 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(req, res) {
    var id, images;
    return _regenerator["default"].wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            id = parseInt(req.params.id);
            _context7.next = 3;
            return (0, _tmdbApi.getMovieImages)(id);

          case 3:
            images = _context7.sent;

            if (images) {
              res.status(200).json(images);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x13, _x14) {
    return _ref8.apply(this, arguments);
  };
}()));
router.post('/:id/reviews', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref9 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee8(req, res) {
    var id, reviews;
    return _regenerator["default"].wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            id = parseInt(req.params.id);
            _context8.next = 3;
            return (0, _tmdbApi.getMovieReviews)(id);

          case 3:
            reviews = _context8.sent;

            if (reviews.id == id) {
              req.body.created_at = new Date();
              req.body.updated_at = new Date();
              req.body.id = (0, _uniqid["default"])();
              reviews.results.push(req.body); //push the new review onto the list

              res.status(201).json(req.body);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x15, _x16) {
    return _ref9.apply(this, arguments);
  };
}()));
router.get('/tmdb/upcoming', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref10 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee9(req, res) {
    var upcomingMovies;
    return _regenerator["default"].wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            _context9.next = 2;
            return (0, _tmdbApi.getUpcomingMovies)();

          case 2:
            upcomingMovies = _context9.sent;
            res.status(200).json(upcomingMovies);

          case 4:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x17, _x18) {
    return _ref10.apply(this, arguments);
  };
}()));
router.get('/tmdb/now_playing', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref11 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee10(req, res) {
    var nowPlayingMovies;
    return _regenerator["default"].wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            _context10.next = 2;
            return (0, _tmdbApi.getNowPlayingMovies)();

          case 2:
            nowPlayingMovies = _context10.sent;
            res.status(200).json(nowPlayingMovies);

          case 4:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));

  return function (_x19, _x20) {
    return _ref11.apply(this, arguments);
  };
}()));
router.get('/tmdb/top_rated', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref12 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee11(req, res) {
    var topRatedMovies;
    return _regenerator["default"].wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _context11.next = 2;
            return (0, _tmdbApi.getTopRatedMovies)();

          case 2:
            topRatedMovies = _context11.sent;
            res.status(200).json(topRatedMovies);

          case 4:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function (_x21, _x22) {
    return _ref12.apply(this, arguments);
  };
}()));
router.get('/tmdb/movies', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref13 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee12(req, res) {
    var movies;
    return _regenerator["default"].wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return (0, _tmdbApi.getMovies)();

          case 2:
            movies = _context12.sent;
            res.status(200).json(movies);

          case 4:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));

  return function (_x23, _x24) {
    return _ref13.apply(this, arguments);
  };
}()));
router.get('/tmdb/:id', (0, _expressAsyncHandler["default"])( /*#__PURE__*/function () {
  var _ref14 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee13(req, res) {
    var id, movie;
    return _regenerator["default"].wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            id = parseInt(req.params.id);
            _context13.next = 3;
            return (0, _tmdbApi.getMovie)(id);

          case 3:
            movie = _context13.sent;

            if (movie) {
              res.status(200).json(movie);
            } else {
              res.status(404).json({
                message: 'The resource you requested could not be found.',
                status_code: 404
              });
            }

          case 5:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));

  return function (_x25, _x26) {
    return _ref14.apply(this, arguments);
  };
}()));
var _default = router;
exports["default"] = _default;